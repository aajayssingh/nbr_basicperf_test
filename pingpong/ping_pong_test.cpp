/**
 * single cacheline 1RTT ping pong.
 * https://www.intel.com/content/dam/www/public/us/en/documents/white-papers/ia-32-ia-64-benchmark-code-execution-paper.pdf
 */

#include <iostream>
#include<thread>
#include <unistd.h>
#include<atomic>
#include <assert.h>

#ifdef __i386
__inline__ uint64_t rdtsc() {
  uint64_t x;
  __asm__ volatile ("rdtsc" : "=A" (x));
  return x;
}
#elif __amd64
__inline__ uint64_t rdtsc() {
  uint64_t a, d;
  __asm__ volatile ("rdtsc" : "=a" (a), "=d" (d));
  return (d<<32) | a;
}
#endif

#define handle_error_en(en, msg) \
               do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)

std::atomic<int> barrier(2);
// std::atomic<bool> ping_pong_var(false);
volatile bool ping_pong_var = false;
char pad[128];
volatile bool done = false;
uint64_t begClock, endClock;
unsigned cycles_low, cycles_high, cycles_low1, cycles_high1;

#define BEGIN_MEASURE(cycles_high, cycles_low) asm volatile (  "CPUID\n\t"\
                "RDTSC\n\t"\
                "mov %%edx, %0\n\t"\
                "mov %%eax, %1\n\t"\
                :"=r" (cycles_high), "=r" (cycles_low)\
                ::"%rax", "%rbx", "%rcx", "%rdx");

#define END_MEASURE(cycles_high1, cycles_low1) asm volatile (  "RDTSCP\n\t"\
                "mov %%edx, %0\n\t"\
                "mov %%eax, %1\n\t"\
                "CPUID\n\t"\
                :"=r" (cycles_high1), "=r" (cycles_low1)\
                ::"%rax", "%rbx", "%rcx", "%rdx");        

void print_cpu_scheduling(int tid)
{
    unsigned int cpu, nd;
    int x = getcpu(&cpu, &nd);
    printf("tid=%d scheduled on cpu=%d, node=%d\n", tid, cpu, nd);
}

void pin_thread_to_cpu(int tid)
{
    //define mask
    cpu_set_t mask;
    CPU_ZERO(&mask);
    CPU_SET(tid, &mask);

    //SCHEDULE affinity of this thread to defined mask
    int s = pthread_setaffinity_np(pthread_self(), sizeof(mask), &mask);
    if (s != 0)
    {
        handle_error_en(s, "pthread_setaffinity_np");
    }
    print_cpu_scheduling(tid);
}

void wait_for_all_threads()
{
    // dec atomic counter and wait for all threads to arrive
    atomic_fetch_sub_explicit(&barrier, 1, std::memory_order_relaxed);
    while (barrier != 0)
    {}
}

void worker(int tid)
{
    pin_thread_to_cpu(tid);

    //ensures that the ping_pong_var is in T1's cache at start.
    if(tid == 1)
        bool dummy = ping_pong_var;

    wait_for_all_threads();

    int numiter = 1000;
    while(numiter--)
    {
        if(1 == tid)
        {
            while(ping_pong_var == false)
            {

            }
            // endClock = rdtsc();
            END_MEASURE(cycles_high1, cycles_low1)

            begClock = ( ((uint64_t)cycles_high << 32) | cycles_low );
            endClock = ( ((uint64_t)cycles_high1 << 32) | cycles_low1 );
            assert(endClock > begClock);
            // std::cout<<"num_clocks="<< (endClock - begClock)<< " duration to wait for sig to get delivered after sending sig="<< durationt<<"\n";
            std::cout<<"num_clocks="<< (endClock - begClock)<<"\n";            
            done = true;
        }
        else
        {
            // begClock = rdtsc();
            BEGIN_MEASURE(cycles_high, cycles_low)
            ping_pong_var = true;

            while(!done) //while t1 disnt do endmeasure
            {}
            done = false;
        }
        ping_pong_var = false;
    }
}


// int main(int argc, const char* argv[]) {
//     constexpr unsigned num_threads = max_threads;
//     std::vector<std::thread> threads(num_threads);

//     for(unsigned i=0;i<num_threads;i++) {
//         threads[i] = std::thread(worker, i);

//         cpu_set_t cpuset;
//         CPU_ZERO(&cpuset);
//         CPU_SET(i, &cpuset);

//         int rc = pthread_setaffinity_np(threads[i].native_handle(), sizeof(cpu_set_t), &cpuset);

//         if(rc != 0) {
//             std::cerr<< "Error calling pthread_setaffinity_np: "<<rc<<"\n";
//         }
//     }

//     for(auto &t : threads) {
//         t.join();
//     }

//     return 0;
// }

int main()
{ 
    unsigned cycles_sublow, cycles_subhigh, cycles_sublow1, cycles_subhigh1;
    uint64_t begsubClock, endsubClock;
    //warmup I cache
    asm volatile (  "CPUID\n\t"
                    "RDTSC\n\t"
                    "mov %%edx, %0\n\t"
                    "mov %%eax, %1\n\t"
                    :"=r" (cycles_subhigh), "=r" (cycles_sublow)
                    ::"%rax", "%rbx", "%rcx", "%rdx");

    asm volatile (  "RDTSCP\n\t"
                    "mov %%edx, %0\n\t"
                    "mov %%eax, %1\n\t"
                    "CPUID\n\t"
                    :"=r" (cycles_subhigh1), "=r" (cycles_sublow1)
                    ::"%rax", "%rbx", "%rcx", "%rdx");    
    asm volatile (  "CPUID\n\t"
                    "RDTSC\n\t"
                    "mov %%edx, %0\n\t"
                    "mov %%eax, %1\n\t"
                    :"=r" (cycles_subhigh), "=r" (cycles_sublow)
                    ::"%rax", "%rbx", "%rcx", "%rdx");

    asm volatile (  "RDTSCP\n\t"
                    "mov %%edx, %0\n\t"
                    "mov %%eax, %1\n\t"
                    "CPUID\n\t"
                    :"=r" (cycles_subhigh1), "=r" (cycles_sublow1)
                    ::"%rax", "%rbx", "%rcx", "%rdx");    

    asm volatile (  "CPUID\n\t"
                    "RDTSC\n\t"
                    "mov %%edx, %0\n\t"
                    "mov %%eax, %1\n\t"
                    :"=r" (cycles_subhigh), "=r" (cycles_sublow)
                    ::"%rax", "%rbx", "%rcx", "%rdx");

    asm volatile (  "RDTSCP\n\t"
                    "mov %%edx, %0\n\t"
                    "mov %%eax, %1\n\t"
                    "CPUID\n\t"
                    :"=r" (cycles_subhigh1), "=r" (cycles_sublow1)
                    ::"%rax", "%rbx", "%rcx", "%rdx");    

    begsubClock = ( ((uint64_t)cycles_subhigh << 32) | cycles_sublow );
    endsubClock = ( ((uint64_t)cycles_subhigh1 << 32) | cycles_sublow1 );
    uint64_t overhead = endsubClock - begsubClock;

    //create two threads
    std::thread t1 (worker, 1);
    std::thread t2 (worker, 2); //change to 94 for numa node 3

    std::cout<<"created threads\n";

    //main thread waits for child threads to finish.
    t1.join();
    t2.join();

    
    // begClock = ( ((uint64_t)cycles_high << 32) | cycles_low );
    // endClock = ( ((uint64_t)cycles_high1 << 32) | cycles_low1 );
    // assert(endClock > begClock);
    // std::cout<<"num_clocks="<< (endClock - begClock)<< " overhead="<< overhead<<"\n";

    return 0;
}