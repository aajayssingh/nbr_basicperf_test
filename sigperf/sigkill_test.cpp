/**
 * one thread sending sig to other cycle measure.
 * https://www.intel.com/content/dam/www/public/us/en/documents/white-papers/ia-32-ia-64-benchmark-code-execution-paper.pdf
 * https://www.ibm.com/docs/en/i/7.1?topic=ssw_ibm_i_71/apis/users_95.htm
 */
#include <iostream>
#include<thread>
#include <unistd.h>
#include<atomic>
#include <assert.h>
#include <signal.h>
#include <cstring>

#define SIGSETPERFTEST 1

#if SIGSETPERFTEST 
#include <setjmp.h>
#endif

#ifdef __i386
__inline__ uint64_t rdtsc() {
  uint64_t x;
  __asm__ volatile ("rdtsc" : "=A" (x));
  return x;
}
#elif __amd64
__inline__ uint64_t rdtsc() {
  uint64_t a, d;
  __asm__ volatile ("rdtsc" : "=a" (a), "=d" (d));
  return (d<<32) | a;
}
#endif

#define handle_error_en(en, msg) \
               do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)

std::atomic<int> barrier(2);
uint64_t begClock, endClock;
unsigned cycles_low, cycles_high, cycles_low1, cycles_high1;

pthread_t thread_tobe_signalled;

volatile bool gflag = false;
volatile bool done = false;
void sighandler(int signo)
{
    gflag = true;
    return;
}

#if SIGSETPERFTEST 
static sigjmp_buf *setjmpbuffers;
#endif

#define BEGIN_MEASURE asm volatile (  "CPUID\n\t"\
                "RDTSC\n\t"\
                "mov %%edx, %0\n\t"\
                "mov %%eax, %1\n\t"\
                :"=r" (cycles_high), "=r" (cycles_low)\
                ::"%rax", "%rbx", "%rcx", "%rdx");

#define END_MEASURE asm volatile (  "RDTSCP\n\t"\
                "mov %%edx, %0\n\t"\
                "mov %%eax, %1\n\t"\
                "CPUID\n\t"\
                :"=r" (cycles_high1), "=r" (cycles_low1)\
                ::"%rax", "%rbx", "%rcx", "%rdx");    

void print_cpu_scheduling(int tid)
{
    unsigned int cpu, nd;
    int x = getcpu(&cpu, &nd);
    printf("tid=%d scheduled on cpu=%d, node=%d\n", tid, cpu, nd);
}

void pin_thread_to_cpu(int tid)
{
    //define mask
    cpu_set_t mask;
    CPU_ZERO(&mask);
    CPU_SET(tid, &mask);

    //SCHEDULE affinity of this thread to defined mask
    int s = pthread_setaffinity_np(pthread_self(), sizeof(mask), &mask);
    if (s != 0)
    {
        handle_error_en(s, "pthread_setaffinity_np");
    }
    print_cpu_scheduling(tid);
}

void wait_for_all_threads()
{
    // dec atomic counter and wait for all threads to arrive
    atomic_fetch_sub_explicit(&barrier, 1, std::memory_order_relaxed);
    while (barrier != 0)
    {}
}

void worker(int tid)
{
    pin_thread_to_cpu(tid);
    if (tid != 1)
    {
        thread_tobe_signalled = pthread_self();
        std::cout<<"tid="<<tid<<" thread_tobe_signalled="<<thread_tobe_signalled<<"\n";
    }

    wait_for_all_threads();
    int numruns = 10000;
    while(numruns--)
    {
        
#if SIGSETPERFTEST  
            uint64_t durationt=0, startt=0;

            if(1 == tid)
            {
                startt = rdtsc();      
                // int status = pthread_kill(thread_tobe_signalled, 0);// no sig is sent to other
                // int status = pthread_kill(pthread_self(), 0);// no sig is sent to self
                // int status = pthread_kill(thread_tobe_signalled, SIGQUIT);// no sig is sent
                // int status = pthread_kill(pthread_self(), SIGQUIT);

                // aj testing the sigset cost begin
                // sigsetjmp(setjmpbuffers[(tid)*8], 1);
                // aj testing the sigset cost end
                int t = 1;
                __sync_bool_compare_and_swap(&t, t, 1);

                durationt = rdtsc() - startt;
            
                std::cout<<"num_clocks="<< (endClock - begClock)<< " duration pkill="<< durationt<<"\n";
            }
#else
        if(1 == tid)
        {
            uint64_t durationt=0, startt=0;
            BEGIN_MEASURE
            // send sigkill to other thread  
            startt = rdtsc();      
            // int status = pthread_kill(thread_tobe_signalled, 0);// no sig is sent to other
            // int status = pthread_kill(pthread_self(), 0);// no sig is sent to self
            int status = pthread_kill(thread_tobe_signalled, SIGQUIT);// no sig is sent
            // int status = pthread_kill(pthread_self(), SIGQUIT);

            durationt = rdtsc() - startt;      
            
            // END_MEASURE
            // if (status != 0)
            // {
            //     handle_error_en(status, "error in sending signal.");
            // }
            // BEGIN_MEASURE

            while(gflag != true)
            {}
            END_MEASURE
            gflag = false;

            begClock = ( ((uint64_t)cycles_high << 32) | cycles_low );
            endClock = ( ((uint64_t)cycles_high1 << 32) | cycles_low1 );
            // assert(endClock > begClock);
            std::cout<<"num_clocks="<< (endClock - begClock)<< " duration pkill="<< durationt<<"\n";
            // std::cout<<"num_clocks="<< (endClock - begClock)<<"\n";            
        }
        else
        {
            while(!done){
                // int ret = usleep(1);
                // if (ret)
                // {
                //     printf("errno %d\n", errno);
                // }
            }

        }
#endif
    
    }
    done = true; //t1 will set done to cause the other thread in else case to stop.
}


int main()
{ 
#if SIGSETPERFTEST 
    setjmpbuffers = new sigjmp_buf[2*8];
#endif

    //sighandler registration.
    struct sigaction actions;
    memset(&actions, 0, sizeof(actions));
    sigemptyset(&actions.sa_mask);
    actions.sa_flags = 0;
    actions.sa_handler = sighandler;
    int rc = sigaction(SIGQUIT, &actions, NULL);
    if (rc != 0)
    {
        handle_error_en(rc, "sigaction error");
    }


    unsigned cycles_sublow, cycles_subhigh, cycles_sublow1, cycles_subhigh1;
    uint64_t begsubClock, endsubClock;
    //warmup I cache
    asm volatile (  "CPUID\n\t"
                    "RDTSC\n\t"
                    "mov %%edx, %0\n\t"
                    "mov %%eax, %1\n\t"
                    :"=r" (cycles_subhigh), "=r" (cycles_sublow)
                    ::"%rax", "%rbx", "%rcx", "%rdx");

    asm volatile (  "RDTSCP\n\t"
                    "mov %%edx, %0\n\t"
                    "mov %%eax, %1\n\t"
                    "CPUID\n\t"
                    :"=r" (cycles_subhigh1), "=r" (cycles_sublow1)
                    ::"%rax", "%rbx", "%rcx", "%rdx");    
    asm volatile (  "CPUID\n\t"
                    "RDTSC\n\t"
                    "mov %%edx, %0\n\t"
                    "mov %%eax, %1\n\t"
                    :"=r" (cycles_subhigh), "=r" (cycles_sublow)
                    ::"%rax", "%rbx", "%rcx", "%rdx");

    asm volatile (  "RDTSCP\n\t"
                    "mov %%edx, %0\n\t"
                    "mov %%eax, %1\n\t"
                    "CPUID\n\t"
                    :"=r" (cycles_subhigh1), "=r" (cycles_sublow1)
                    ::"%rax", "%rbx", "%rcx", "%rdx");    

    asm volatile (  "CPUID\n\t"
                    "RDTSC\n\t"
                    "mov %%edx, %0\n\t"
                    "mov %%eax, %1\n\t"
                    :"=r" (cycles_subhigh), "=r" (cycles_sublow)
                    ::"%rax", "%rbx", "%rcx", "%rdx");

    asm volatile (  "RDTSCP\n\t"
                    "mov %%edx, %0\n\t"
                    "mov %%eax, %1\n\t"
                    "CPUID\n\t"
                    :"=r" (cycles_subhigh1), "=r" (cycles_sublow1)
                    ::"%rax", "%rbx", "%rcx", "%rdx");    

    begsubClock = ( ((uint64_t)cycles_subhigh << 32) | cycles_sublow );
    endsubClock = ( ((uint64_t)cycles_subhigh1 << 32) | cycles_sublow1 );
    uint64_t overhead = endsubClock - begsubClock;

    //create two threads
    std::thread t1 (worker, 1);
    std::thread t2 (worker, 128); //change to 94 for numa node 3

    std::cout<<"created threads\n";

    //main thread waits for child threads to finish.
    t1.join();
    t2.join();

    
    // begClock = ( ((uint64_t)cycles_high << 32) | cycles_low );
    // endClock = ( ((uint64_t)cycles_high1 << 32) | cycles_low1 );
    // assert(endClock > begClock);
    // std::cout<<"num_clocks="<< (endClock - begClock)<< " overhead="<< overhead<<"\n";

    return 0;
}