# nbr_basicperf_test

repo is to design basic perf tests for NBR.

## pingpong test
Main thread creates two child pthreads: tid1 and tid2. 
tid1 waits for a global variable to change from false to true.
tid2 changes the global variable to true.
cycles consumed between tid2 sets the golabl var to true and tid1 observes the change is measuyred using tdsc counter.

# Compile
~/> make
# run
~/> for i in {1..10}; do ./pingpongtest|grep "num_clocks";done


## sigperftest
Main thread creates two child pthreads: tid1 and tid2. 

tid1 sends SIGUSR1 to tid2 via pthread_kill().
cycles consumed to to execute pthread_kill() are measured using rdtsc
# Compile
~/> make
# run
~/> for i in {1..10}; do ./sigtest|grep "num_clocks";done

