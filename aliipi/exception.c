#include <stdio.h>
#include <stdlib.h>
#include <stdatomic.h>

#include <sys/param.h>
// #include <sys/cpuset.h>

#include <unistd.h>
#include <pthread.h>
#include <signal.h>

#include <x86intrin.h>

volatile uint64_t recv;

void
sighandler(int signo)
{
    recv = __builtin_readcyclecounter();
}

void *
sec_thread(void *arg)
{
    signal(SIGTRAP, sighandler);

    for (int i = 0; i < 100; i++) {
	uint64_t start = __builtin_readcyclecounter();
	asm("int3");
	uint64_t stop = __builtin_readcyclecounter();

	printf("exc %lu ret %lu total %lu\n", recv - start, stop - recv, stop - start);
    }

    return NULL;
}

int
main(int argc, const char *argv[])
{
    sec_thread(NULL);

    return 0;
}